(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

;; CI - For asdf:test-system to quit with a useful exit code for Parachute systems,
;; set cl-user::*exit-on-test-failures* to T. 
;; This'll cause any invocation of test to behave like test-toplevel.
;; See documentation : https://github.com/Shinmera/parachute

(setf cl-user::*exit-on-test-failures* t)

(ql:quickload "cl-cas")  
(asdf:test-system "cl-cas")